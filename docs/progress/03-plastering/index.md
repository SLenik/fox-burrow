# Этап 3: Штукатурка стен

 - **Описание (по приложению к Договору)**: включает в себя работы по формированию геометрии стен при установке маяков везде, где это возможно. Грунтовку, бетоноконтакт, сетку (при необходимости) и т.д. и саму штукатурку стен. 
 - **Конечный результат (по приложению к Договору)**: оштукатуренные стены в срок согласно регламенту компании.

### Исходная схема откосов из техпроекта

![Схема откосов](./techdesign-16-window-slopes.png "Схема откосов")

### Предварительно согласованные корректировки к смете

Работы по этапу будут выполняться по утверждённой предварительной смете с учётом следующих изменений:

1. В процессе выполнения этапа мастерами-оконщиками будет произведена замена старых окон на лоджии и в спальне на новые.
1. Вместо битумной ленты мастерами при установке окон будет использован СТИЗ.
1. На лоджии работы по оштукатуриванию производиться не будут.
1. Откосы у всех окон будут штукатурные (вместо использования сэндвич-панелей).

## Завершение

1. Работы по нанесению штукатурки были выполнены. Акт подписан 30.10.2023.
1. В дальнейшем было обнаружено, что один маяк в гостиной забыли удалить - их удалили перед этапом нанесения шпатлевки и шлифовки стен.

### Галерея

<video src=".\Отчёт 20231024_123359.mp4" preload="metadata" height="512" controls>
  Sorry, your browser doesn't support embedded videos, but don't worry, you can
  <a href=".\Отчёт 20231024_123359.mp4">download it</a>
  and watch it with your favorite video player!
</video>

<p>
  <p float="left" align="center">
    <img src="./Спальня IMG-20231020-WA0023.jpg" width="300" />
    <img src="./Спальня 20231024_124137.jpg" width="300" />
    <img src="./Спальня IMG-20231024-WA0055.jpg" width="300" />
    <img src="./Спальня IMG-20231024-WA0057.jpg" width="300" />
  </p>
  <figcaption align="center">Спальня</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Гостиная IMG-20231013-WA0047.jpg" width="300" />
    <img src="./Гостиная 20231016_122952.jpg" width="300" />
    <img src="./Гостиная 20231016_122959.jpg" width="300" />
    <img src="./Гостиная IMG-20231020-WA0018.jpg" width="300" />
    <img src="./Гостиная IMG-20231020-WA0019.jpg" width="300" />
    <img src="./Гостиная IMG-20231024-WA0053.jpg" width="300" />
    <img src="./Гостиная IMG-20231025-WA0067.jpg" width="300" />
  </p>
  <figcaption align="center">Гостиная</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Детская IMG-20231013-WA0048.jpg" width="300" />
    <img src="./Детская IMG-20231020-WA0020.jpg" width="300" />
    <img src="./Детская 20231024_124116.jpg" width="300" />
    <img src="./Детская IMG-20231024-WA0054.jpg" width="300" />
  </p>
  <figcaption align="center">Детская</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Холл 20231016_122638.jpg" width="300" />
    <img src="./Холл IMG-20231017-WA0068.jpg" width="300" />
    <img src="./Холл IMG-20231018-WA0080.jpg" width="300" />
    <img src="./Холл IMG-20231018-WA0081.jpg" width="300" />
    <img src="./Холл IMG-20231013-WA0046.jpg" width="300" />
    <img src="./Холл IMG-20231019-WA0117.jpg" width="300" />
  </p>
  <figcaption align="center">Холл</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Коридор IMG-20231013-WA0045.jpg" width="300" />
    <img src="./Коридор 20231016_122635.jpg" width="300" />
    <img src="./Коридор IMG-20231017-WA0065.jpg" width="300" />
    <img src="./Коридор IMG-20231018-WA0079.jpg" width="300" />
    <img src="./Коридор IMG-20231019-WA0118.jpg" width="300" />
    <img src="./Коридор 20231024_123624.jpg" width="300" />
  </p>
  <figcaption align="center">Коридор</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Туалет IMG-20231018-WA0083.jpg" width="300" />
    <img src="./Туалет 20231024_123633.jpg" width="300" />
  </p>
  <figcaption align="center">Туалет</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Ванная IMG-20231018-WA0082.jpg" width="300" />
  </p>
  <figcaption align="center">Ванная</figcaption>
</p>
<p>
  <p float="left" align="center">
    <img src="./Кухня IMG-20231013-WA0043.jpg" width="300" />
    <img src="./Кухня IMG-20231013-WA0044.jpg" width="300" />
    <img src="./Кухня IMG-20231018-WA0077.jpg" width="300" />
    <img src="./Кухня IMG-20231018-WA0078.jpg" width="300" />
    <img src="./Кухня 20231024_123631.jpg" width="300" />
  </p>
  <figcaption align="center">Кухня</figcaption>
</p>
